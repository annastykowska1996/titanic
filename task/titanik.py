import pandas as pd
import os


def get_titatic_dataframe() -> pd.DataFrame:
    file_path = 'train.csv'
    df = pd.read_csv(file_path)
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    
    # extract title out of names
    df['Title'] = df['Name'].apply(lambda x: 'Mr.' if 'mr.' in x.lower() else 'Mrs.' if 'mrs.' in x.lower() else 'Miss.' if 'miss.' in x.lower() else None)
    
    # group DataFrame by extracted Title
    results = []
    for title, df_chunk in df.groupby('Title'):
        # calculate missing values
        missing = df_chunk['Age'].isna().sum()
        # calculate mean and round to nearest integer
        mean = df_chunk['Age'].mean().round(0).astype('int')
        # format result
        result = (title, missing, mean)
        # append result to result set
        results.append(result)
    
    # There are a number of names that does not fit under groups, whereas for males it's easy to classified it under Mr. for females it's impossible to determine the martial status and assing according title. 
    # The missing titles are not included in the result as it wasn't mentioned to classify them.
    
    return results